package pt.ez.lmr.password;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBPasswordAdapter {

	private DBHelper dbHelper;
	public static final String TABLE = "passwords";
	public static final String _ID = "_id";
	public static final String DESTINATION = "destination";
	public static final String USER_NAME = "userName";
	public static final String PASSWORD = "password";
	public static final String SELECT_PASSWORDS = "SELECT * FROM " + TABLE;

	/**
	 * ! Creates a new instance of DBPasswordAdapter
	 * 
	 * @param context The creation context
	 */
	public DBPasswordAdapter(Context context) {
		dbHelper = new DBHelper(context, TABLE, _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," + DESTINATION
				+ " TEXT," + USER_NAME + " TEXT, " + PASSWORD + " TEXT");
	}

	/**
	 * ! Insert new password set to the table
	 * 
	 * @param pass The password set
	 * @return True if success, false if it fails
	 */
	public boolean insertPassword(PasswordSet pass) {
		try {
			SQLiteDatabase sqliteDB = dbHelper.getWritableDatabase();
			ContentValues initialValues = new ContentValues();
			// initialValues.put(_ID, Id);
			initialValues.put(DESTINATION, pass.getDestination());
			initialValues.put(USER_NAME, pass.getUserName());
			initialValues.put(PASSWORD, pass.getPassword());
			sqliteDB.insert(TABLE, null, initialValues);
			sqliteDB.close();
		} catch (SQLException sqlerror) {
			Log.v("Insert into table error", sqlerror.getMessage());
			
			return false;
		}
		return true;
	}

	/**
	 * ! Update password set
	 * 
	 * @param pass The password set to update
	 * @return True if updates, false if it fails
	 */
	public boolean updatePassword(PasswordSet pass) {
		try {
			SQLiteDatabase sqliteDB = dbHelper.getWritableDatabase();
			String _id = Integer.toString(pass.getId());
			ContentValues initialValues = new ContentValues();
			// initialValues.put(_ID, Id);
			initialValues.put(DESTINATION, pass.getDestination());
			initialValues.put(USER_NAME, pass.getUserName());
			initialValues.put(PASSWORD, pass.getPassword());
			sqliteDB.update(TABLE, initialValues, _ID + "=" + _id, null);
			sqliteDB.close();
		} catch (SQLException sqlerror) {
			Log.v("Update table error", sqlerror.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * ! Delete password 
	 * 
	 * @param _id The password id
	 * @return True if updates, false otherwise
	 */
	public boolean deletePassword(int _id){
		try {
			SQLiteDatabase sqliteDB = dbHelper.getWritableDatabase();
			sqliteDB.delete(TABLE, _ID + "=" + _id, null);
			sqliteDB.close();
		} catch (SQLException sqlerror) {
			Log.v("Delete from table error", sqlerror.getMessage());
			return false;
		}
		return true;
		
	}

	/**
	 * ! Get all password set's from database
	 * 
	 * @return Array List with all the password set's into database
	 */
	public ArrayList<PasswordSet> getPasswords() {
		SQLiteDatabase sqliteDB = dbHelper.getWritableDatabase();
		ArrayList<PasswordSet> ps = new ArrayList<PasswordSet>();
		Cursor crsr = sqliteDB.rawQuery(SELECT_PASSWORDS, null);
		crsr.moveToFirst();
		for (int i = 0; i < crsr.getCount(); i++) {
			ps.add(new PasswordSet(crsr.getInt(0), crsr.getString(1), crsr
					.getString(2), crsr.getString(3)));
			crsr.moveToNext();
		}
		sqliteDB.close();
		return ps;
	}

	/**
	 * ! Get a given password set
	 * 
	 * @param _id The password set id's
	 * @return The password set
	 */
	public PasswordSet getPassword(int _id) {
		SQLiteDatabase sqliteDB = dbHelper.getWritableDatabase();
		String s = "SELECT * FROM " + TABLE + " WHERE " + _ID + "=" + _id;
		Cursor crsr = sqliteDB.rawQuery(s, null);
		crsr.moveToFirst();
		PasswordSet ps = new PasswordSet(crsr.getInt(0), crsr.getString(1),
				crsr.getString(2), crsr.getString(3));
		sqliteDB.close();
		return ps;
	}

}
