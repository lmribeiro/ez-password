package pt.ez.lmr.password;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PasswordListAdapter extends BaseAdapter{
	
	private List<PasswordSet> items;
	private String SEED = "jamesBond";

	public PasswordListAdapter(final Context context, final int itemResId,final List<PasswordSet> items) {
			this.items = items;
	}

	public int getCount() {
		return this.items.size();
	}

	public Object getItem(int arg0) {
		return this.items.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		final PasswordSet row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.password_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        TextView destination = (TextView) itemView.findViewById(R.id.passwordItemDestination);
        destination.setText(SimpleCrypto.decrypt(SEED, row.getDestination()));
        
        return itemView;
	}

}
