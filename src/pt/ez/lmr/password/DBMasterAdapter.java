package pt.ez.lmr.password;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBMasterAdapter {

	private DBHelper dbHelper;
	private static final String TABLE = "masterkey";
	private static final String _ID = "_id";
	private static final String MASTER_KEY = "master";

	/**
	 * ! Creates a new instance of DBMasterAdapter
	 * 
	 * @param context
	 *            The creation context
	 */
	public DBMasterAdapter(Context context) {
		dbHelper = new DBHelper(context, TABLE, _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," + MASTER_KEY + " TEXT");
	}

	/**
	 * ! Set the Master Key
	 * 
	 * @param key
	 *            The Master Key
	 * @return True if success, false if it fails
	 */
	public long setMasterKey(String key) {
		long result = -1;
		try {
			SQLiteDatabase sqlite = dbHelper.getWritableDatabase();
			ContentValues initialValues = new ContentValues();
			// initialValues.put(_ID, Id);
			initialValues.put(MASTER_KEY, key);
			result = sqlite.insert(TABLE, null, initialValues);
			sqlite.close();
		} catch (SQLException sqlerror) {
			Log.v("Insert into table error", sqlerror.getMessage());
			System.out.println("Insert into table error");
			return -1;
		}
		return result;
	}

	/**
	 * ! Update the Master Key
	 * 
	 * @param key
	 *            The new Master Key
	 * @return True if updates, false if it fails to update
	 */
	public boolean updateMasterKey(String key) {
		try {
			String _id = "1";
			SQLiteDatabase sqlite = dbHelper.getWritableDatabase();
			ContentValues initialValues = new ContentValues();
			// initialValues.put(_ID, Id);
			initialValues.put(MASTER_KEY, key);
			sqlite.update(TABLE, initialValues, _id, null);
			sqlite.close();
		} catch (SQLException sqlerror) {
			Log.v("Update table error", sqlerror.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * ! Get the Master key
	 * 
	 * @return The Master Key
	 */
	public String getMasterKey() {
		String key = "error";
		SQLiteDatabase sqliteDB = dbHelper.getReadableDatabase();
		String s = "SELECT * FROM " + TABLE;
		Cursor crsr = sqliteDB.rawQuery(s, null);
		crsr.moveToFirst();
		sqliteDB.close();
		
		if (crsr.getCount() > 0){
			key = crsr.getString(1);
		}
		return key;
	}
}
