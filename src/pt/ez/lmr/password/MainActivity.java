package pt.ez.lmr.password;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static DBMasterAdapter masterAdapter;
	private String SEED = "jamesBond";
	private String key = "error";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		masterAdapter = new DBMasterAdapter(this);
		String Encriptedkey = masterAdapter.getMasterKey();
		if (Encriptedkey != "error"){
			key = SimpleCrypto.decrypt(SEED, Encriptedkey);
		}

		if (key != "error") {

			// Set the layout
			setContentView(R.layout.activity_start_login);

			ImageButton Start = (ImageButton) findViewById(R.id.imageButtonEnter);
			Start.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					EditText etMaster = (EditText) findViewById(R.id.editTextLogin);
					String s = etMaster.getText().toString();

					if (s.compareTo(key) == 0) {
						Intent save = new Intent(getApplicationContext(),
								PasswordListActivity.class);
						startActivityForResult(save, 1);
					}else{
						etMaster.setText("");
						Toast.makeText(getApplicationContext(),
								"Wrong Password!\n Access denied.", Toast.LENGTH_SHORT)
								.show();
					}
				}
			});
		} else {

			// Set the layout
			setContentView(R.layout.activity_start_first_login);

			Button Save = (Button) findViewById(R.id.mainButtonSave);
			Save.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					EditText mk1 = (EditText) findViewById(R.id.editTextMasterKey1);
					String master1 = mk1.getText().toString();

					EditText mk2 = (EditText) findViewById(R.id.editTextMasterKey2);
					String master2 = mk2.getText().toString();

					if ((master1.compareTo(master2) == 0) && (master1.compareTo("") != 0)) {
						 System.out.println(master1);
						 masterAdapter.setMasterKey(SimpleCrypto.encrypt(SEED, master1));

						Intent save = new Intent(getApplicationContext(),
								PasswordListActivity.class);
						startActivityForResult(save, 1);

						Toast.makeText(getApplicationContext(),
								R.string.key_defined, Toast.LENGTH_SHORT)
								.show();
					} else {
						Toast.makeText(getApplicationContext(),
								R.string.fail_master_key, Toast.LENGTH_SHORT)
								.show();
						mk1.requestFocus();
						mk1.setText("");
						mk2.setText("");
					}
				}
			});

			Button Cancel = (Button) findViewById(R.id.mainButtonCancel);
			Cancel.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();

				}
			});
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_CANCELED:
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return (super.onPrepareOptionsMenu(menu));

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// check selected menu item
		switch (item.getItemId()) {
		case R.id.menu_about:
			Intent aboutIntent = new Intent(this, AboutActivity.class);
			startActivity(aboutIntent);
			return true;

		case R.id.menu_help:
			// Intent helpIntent = new Intent(this, HelpActivity.class);
			// startActivity(helpIntent);
			Toast.makeText(getApplicationContext(), "Not impelemented",
					Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

}
