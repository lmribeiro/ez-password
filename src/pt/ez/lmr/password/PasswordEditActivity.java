package pt.ez.lmr.password;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordEditActivity extends Activity {
	
	private DBPasswordAdapter adapter;
	protected TextView destination;
    protected TextView username;
    protected TextView password;
    protected int _id;
    private String SEED = "jamesBond";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_add);
		
        adapter = new DBPasswordAdapter(getApplicationContext());
        _id = getIntent().getIntExtra("ID", 0);
        PasswordSet p = adapter.getPassword(_id);
        destination = (TextView) findViewById(R.id.add_new_destination);
        destination.setText(SimpleCrypto.decrypt(SEED, p.getDestination()));
        username = (TextView) findViewById(R.id.add_new_username);
        username.setText(SimpleCrypto.decrypt(SEED, p.getUserName()));
        password = (TextView) findViewById(R.id.add_new_password);
        password.setText(SimpleCrypto.decrypt(SEED, p.getPassword()));
        
        Button Save = (Button) findViewById(R.id.addNewButtonSave);
		Save.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				PasswordSet p = new PasswordSet(_id, 
						SimpleCrypto.encrypt(SEED, destination.getText().toString()), 
						SimpleCrypto.encrypt(SEED, username.getText().toString()), 
						SimpleCrypto.encrypt(SEED, password.getText().toString()));
				adapter.updatePassword(p);
				setResult(Activity.RESULT_OK);
				finish();
			}
		});

		Button Cancel = (Button) findViewById(R.id.addNewButtonCancel);
		Cancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				finish();
			}
		});
		
	}
	
	/******************** Options menu **************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return (super.onPrepareOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// check selected menu item
		switch (item.getItemId()) {

		case R.id.menu_about:
			Intent credits = new Intent(this, AboutActivity.class);
			startActivity(credits);
			return true;

		case R.id.menu_help:
			// Intent helpIntent = new Intent(this, HelpActivity.class);
			// startActivity(helpIntent);
			Toast.makeText(getApplicationContext(), "Not impelemented",
					Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
	/******************** Options menu end ***********************/

}
