package pt.ez.lmr.password;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class PasswordListActivity extends ListActivity {

	protected ListView passwordList;
	private PasswordSet currentPassword;
	private DBPasswordAdapter dbAdapter;
	private PasswordListAdapter listAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_list);

		/**
		 * ! Create database if it is not created yet
		 */
		dbAdapter = new DBPasswordAdapter(getApplicationContext());

		/**
		 * ! Fill the list view
		 */
		this.fillListView();

		Button New = (Button) findViewById(R.id.buttonAdd);
		New.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent addNew = new Intent(PasswordListActivity.this,
						PasswordAddActivity.class);
				startActivityForResult(addNew, 1);
			}
		});

		/**
		 * ! Set the view to appear on the context menu
		 */
		registerForContextMenu(getListView());

		/**
		 * ! Register a callback to be invoked when an item in this AdapterView
		 * has been clicked.
		 */
		passwordList.setOnItemClickListener(new OnItemClickListener() {
			/*
			 * ! Callback method to be invoked when an item in this AdapterView
			 * has been clicked. Implementers can call
			 * getItemAtPosition(position) if they need to access the data
			 * associated with the selected item. Parameters arg0 The
			 * AdapterView where the click happened. arg1 The view within the
			 * AdapterView that was clicked (this will be a view provided by the
			 * adapter) arg2 The position of the view in the adapter. arg3 The
			 * row id of the item that was clicked.
			 */
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(PasswordListActivity.this,
						PasswordDetailsActivity.class);
				currentPassword = (PasswordSet) listAdapter.getItem(arg2);
				intent.putExtra("ID", currentPassword.getId());
				startActivityForResult(intent, 1);
			}
		});

	}

	/**
	 * Change the BackPressed to force the application to close
	 */
	@Override
	public void onBackPressed() {
		setResult(Activity.RESULT_CANCELED);
		finish();
	}

	/*
	 * ! Fill the list view
	 */
	private void fillListView() {
		passwordList = new ListView(this);
		passwordList = getListView();
		ArrayList<PasswordSet> pass = dbAdapter.getPasswords();

		if (pass.size() > 0) {
			listAdapter = new PasswordListAdapter(getApplicationContext(),
					R.layout.password_item, pass);
			passwordList.setAdapter(listAdapter);

		} else {
			listAdapter = new PasswordListAdapter(getApplicationContext(),
					R.layout.password_item, pass);
			passwordList.setAdapter(listAdapter);
			Toast.makeText(getApplicationContext(), R.string.reg_info,
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * ! On activity result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_OK:
			this.fillListView();
			break;

		case RESULT_CANCELED:
			Toast.makeText(getApplicationContext(), R.string.op_canceled,
					Toast.LENGTH_SHORT).show();
			this.fillListView();
			break;
		}
	}

	/************************ Options Menu ******************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_password_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// check selected menu item

		switch (item.getItemId()) {
		case R.id.password_list_menu_add_new:
			Intent addNewIntent = new Intent(this, PasswordAddActivity.class);
			startActivityForResult(addNewIntent, 1);
			return true;

		case R.id.password_list_menu_master_key:
			Intent masterKeyIntent = new Intent(this,
					ChangeMasterKeyActivity.class);
			startActivity(masterKeyIntent);
			return true;

		case R.id.password_list_menu_about:
			Intent aboutIntent = new Intent(this, AboutActivity.class);
			startActivity(aboutIntent);
			return true;

		case R.id.password_list_menu_help:
			// Intent helpIntent = new Intent(this, HelpActivity.class);
			// startActivity(helpIntent);
			Toast.makeText(getApplicationContext(), "Not impelemented",
					Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	/************************ Options Menu Close *************************/

	/************************ Context Menu *******************************/

	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.edit_delete, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		PasswordSet p = (PasswordSet) listAdapter.getItem(info.position);
		switch (item.getItemId()) {
		case R.id.menu_edit:
			Intent edit = new Intent(this, PasswordEditActivity.class);
			edit.putExtra("ID", p.getId());
			startActivityForResult(edit, 1);
			return true;
		case R.id.menu_delete:
			dbAdapter.deletePassword((int) p.getId());

			Toast.makeText(getApplicationContext(), R.string.password_deleted,
					Toast.LENGTH_SHORT).show();
			this.fillListView();
			return true;

		default:
			return super.onContextItemSelected(item);
		}

	}
	/************************ Context Menu Close ***********************/
}
