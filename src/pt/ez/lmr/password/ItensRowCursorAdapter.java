package pt.ez.lmr.password;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItensRowCursorAdapter extends CursorAdapter {

	public ItensRowCursorAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);

	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView description = (TextView) view
				.findViewById(R.id.passwordItemDestination);

		description.setText(cursor.getString(cursor
				.getColumnIndex(DBPasswordAdapter.DESTINATION)));

	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.activity_password_list, parent,
				false);
		bindView(v, context, cursor);
		return v;
	}
}
