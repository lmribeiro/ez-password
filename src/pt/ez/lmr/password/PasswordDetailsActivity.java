package pt.ez.lmr.password;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordDetailsActivity extends Activity {

	private DBPasswordAdapter adapter;
	private PasswordSet p;
	protected TextView destination;
	protected TextView username;
	protected TextView password;
	protected int _id;
	private String SEED = "jamesBond";
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_details);

		/**
		 * ! Create the adapter
		 */
		adapter = new DBPasswordAdapter(getApplicationContext());

		/**
		 * ! Get the password id
		 */
		_id = getIntent().getIntExtra("ID", 0);

		/**
		 * ! Fill the views
		 */
		this.fillViews();
	}

	/******************* Options Menu *******************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_password_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// check selected menu item
		switch (item.getItemId()) {
		case R.id.password_details_menu_edit:
			Intent edit = new Intent(this, PasswordEditActivity.class);
			edit.putExtra("ID", _id);
			startActivityForResult(edit, 1);
			return true;

		case R.id.password_details_menu_delete:
			adapter.deletePassword(_id);
			Toast.makeText(getApplicationContext(), R.string.password_deleted,
					Toast.LENGTH_SHORT).show();
			setResult(Activity.RESULT_OK);
			finish();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	/******************* Options Menu end ****************************/

	/**
	 * ! On activity result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_OK:
			this.fillViews();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * ! Change the BackPressed
	 */
	@Override
	public void onBackPressed() {
		setResult(Activity.RESULT_OK);
		finish();
	}

	/**
	 * ! Private method to fill the views
	 */
	private void fillViews() {
		p = adapter.getPassword(_id);
		destination = (TextView) findViewById(R.id.password_detail_destination);
		destination.setText(SimpleCrypto.decrypt(SEED, p.getDestination()));
		username = (TextView) findViewById(R.id.password_detail_username);
		username.setText(SimpleCrypto.decrypt(SEED, p.getUserName()));
		password = (TextView) findViewById(R.id.password_detail_password);
		password.setText(SimpleCrypto.decrypt(SEED, p.getPassword()));
	}
}
