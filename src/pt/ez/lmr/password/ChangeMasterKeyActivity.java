package pt.ez.lmr.password;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangeMasterKeyActivity extends Activity {

	private static DBMasterAdapter masterAdapter;
	private String SEED = "jamesBond";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_master_key);

		masterAdapter = new DBMasterAdapter(this);
        
        final EditText mk1 = (EditText) findViewById(R.id.editTextMasterKey1);
        final EditText mk2 = (EditText) findViewById(R.id.editTextMasterKey2);

		Button Save = (Button) findViewById(R.id.mainButtonSave);
		Save.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String master1 = mk1.getText().toString();
				String master2 = mk2.getText().toString();

				if (master1.compareTo(master2) == 0 && master1 != ""
						&& master1 != null) {
					System.out.println(master1);
					masterAdapter.setMasterKey(SimpleCrypto.encrypt(SEED,
							master1));

					Intent save = new Intent(getApplicationContext(),
							PasswordListActivity.class);
					startActivityForResult(save, 1);

					Toast.makeText(getApplicationContext(),
							R.string.key_defined, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.fail_master_key, Toast.LENGTH_SHORT)
							.show();
					mk1.requestFocus();
					mk1.setText("");
					mk2.setText("");
				}
			}
		});

		Button Cancel = (Button) findViewById(R.id.mainButtonCancel);
		Cancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();

			}
		});
	}

}
