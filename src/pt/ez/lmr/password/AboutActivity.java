package pt.ez.lmr.password;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		TextView tvAppVersion = (TextView) findViewById(R.id.appVersion);
		tvAppVersion.setText(getVersionName(this, AboutActivity.class));

	}

	/**
	 * Get application Version
	 * 
	 * @param context The application context
	 * @param cls The class
	 * @return String with the application version
	 */
	public String getVersionName(Context context, Class<AboutActivity> cls) {
		try {
			ComponentName comp = new ComponentName(context, cls);
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(
					comp.getPackageName(), 0);
			return pinfo.versionName;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return null;
		}
	}
}
