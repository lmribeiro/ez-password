package pt.ez.lmr.password;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordAddActivity extends Activity {

	private DBPasswordAdapter adapter;
	protected TextView destination;
	protected TextView username;
	protected TextView password;
	private String SEED = "jamesBond";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_add);

		adapter = new DBPasswordAdapter(this);
		destination = (TextView) findViewById(R.id.add_new_destination);
		username = (TextView) findViewById(R.id.add_new_username);
		password = (TextView) findViewById(R.id.add_new_password);

		Button Save = (Button) findViewById(R.id.addNewButtonSave);
		Save.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				PasswordSet p = new PasswordSet(0, 
						SimpleCrypto.encrypt(SEED, destination.getText().toString()), 
						SimpleCrypto.encrypt(SEED, username.getText().toString()),
						SimpleCrypto.encrypt(SEED, password.getText().toString()));
				adapter.insertPassword(p);
				setResult(Activity.RESULT_OK);
				finish();
			}
		});

		Button Cancel = (Button) findViewById(R.id.addNewButtonCancel);
		Cancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				finish();

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return (super.onPrepareOptionsMenu(menu));

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// check selected menu item
		switch (item.getItemId()) {

		case R.id.menu_about:
			Intent credits = new Intent(this, AboutActivity.class);
			startActivity(credits);
			return true;

		case R.id.menu_help:
			// Intent helpIntent = new Intent(this, HelpActivity.class);
			// startActivity(helpIntent);
			Toast.makeText(getApplicationContext(), "Not impelemented",
					Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
