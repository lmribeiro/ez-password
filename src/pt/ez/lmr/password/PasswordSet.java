package pt.ez.lmr.password;

public class PasswordSet {

	private int id;
	private String destination;
	private String userName;
	private String password;

	public PasswordSet(int id, String destination, String userName,
			String password) {
		super();
		this.id = id;
		this.destination = destination;
		this.userName = userName;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
